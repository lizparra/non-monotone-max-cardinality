import java.util.*;
import java.io.*;
import java.lang.*;

class WeightedGraph {
    private int numberOfVertices;
    private HashMap<String, Node> nodes;

    public WeightedGraph(int numberOfVertices){
        this.numberOfVertices = numberOfVertices;
        this.nodes = new HashMap<>();
    }

    public HashMap<String, Node> getNodes(){
        return nodes;
    }

    public int getNumberOfVertices(){
        return numberOfVertices;
    }

    public static void main(String[] args){
        String[] data;
        WeightedGraph graph;
        Node source;
        Node destination;
        double weight;
        Edge edge;
        String[] n;
        List<Node> nodes = new ArrayList<>();

        try {
            File file = new File("input.txt");
            Scanner scan = new Scanner(file);
            graph = new WeightedGraph(scan.nextInt());
            scan.nextLine();
            n = scan.nextLine().split(" ");

            while (scan.hasNextLine()) {
                data = scan.nextLine().split(" ");

                if(graph.getNodes().containsKey(data[0])){
                    source = graph.getNodes().get(data[0]);
                } else {
                    source = new Node(data[0]);
                    graph.getNodes().put(data[0], source);
                }
                
                if(graph.getNodes().containsKey(data[1])){
                    destination = graph.getNodes().get(data[1]);
                } else {
                    destination = new Node(data[1]);
                    graph.getNodes().put(data[1], destination);
                }

                weight = Double.parseDouble(data[2]);

                edge = new Edge(destination, weight);
                source.addEdge(edge);
                edge = new Edge(source, weight);
                destination.addEdge(edge);
            }

            for(String str : n){
                nodes.add(graph.getNodes().get(str));
            }

            System.out.println("Max Influence: " + maxInfluence(graph, nodes));

            scan.close();

        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static int bfs(WeightedGraph graph, List<Node> nodes){
        Node n = nodes.get(0);
        int influencedNodes = 0;

        HashSet<Node> visited = new HashSet<>();

        Queue<Node> queue = new LinkedList<>();
        queue.add(n);
        visited.add(n);
        Node s;

        while(!queue.isEmpty()){
            s = queue.poll();
            influencedNodes++;
            visited.add(s);

            for(Edge edge : s.getEdges()){
                if(!visited.contains(edge.getDestination())){
                    queue.add(edge.getDestination());
                } 
            }
        }

        return influencedNodes;
    }

    public static int maxInfluence(WeightedGraph graph, List<Node> nodes){
        WeightedGraph modelGraph = graph.clone();
        WeightedGraph newGraph = getNewGraph(modelGraph);
        Node node;

        System.out.println("ORIGINAL GRAPH: ");

        for(String str : graph.getNodes().keySet()){
            node = graph.getNodes().get(str);
            System.out.println("Node: " + node.getData());
            for(Edge edge : node.getEdges()){
                System.out.println("Edges: " + edge.getDestination().getData() + " " + edge.getWeight());
            }
        }

        System.out.println();
        System.out.println("NEW GRAPH: ");

        for(String str : newGraph.getNodes().keySet()){
            node = newGraph.getNodes().get(str);
            System.out.println("Node: " + node.getData());
            for(Edge edge : node.getEdges()){
                System.out.println("Edges: " + edge.getDestination().getData() + " " + edge.getWeight());
            }
        }

        int influencedNodes = bfs(graph, nodes);

        return influencedNodes;
    }

    public static WeightedGraph getNewGraph(WeightedGraph graph){
        WeightedGraph newGraph = graph;
        HashMap<Node, List<Edge>> remove = new HashMap<>();
        Node node;
        Random rand = new Random();
        Double chance;
        List<Edge> edges; 

        for(String str : newGraph.getNodes().keySet()){
            node = newGraph.getNodes().get(str);
            edges = new ArrayList<>();

            for(Edge edge : node.getEdges()){
                // chance = rand.nextDouble();

                if(0.5 < edge.getWeight()){
                    edges.add(edge);
                }
            }

            remove.put(node, edges);
        }

        for(Node n : remove.keySet()){
            for(Edge edge : remove.get(n)){
                n.removeEdge(edge);
                edge.getDestination().removeEdge(n);
            }
        }

        return newGraph;
    }

    // copy constructor not work
    // deep copy not work
    // clone function overriding not work :(
    // aaahhhhhh!! :(
    
    // @Override
    // protected WeightedGraph clone() {
    //     WeightedGraph cloned = null;
    //     try {
    //         cloned = (WeightedGraph) super.clone();
    //         cloned.nodes.edges = nodes.edges;
    //         cloned.nodes = nodes;
    //     } catch (CloneNotSupportedException cns){
    //         System.out.println("Error while cloning.");
    //     }
    //     return cloned;
    // }
}

class Edge {
    private Node destination;
    private double weight;

    public Edge(Node destination, double weight){
        this.destination = destination;
        this.weight = weight;
    }

    public Node getDestination(){
        return destination;
    }

    public double getWeight(){
        return weight;
    }
}
    
class Node {
    private String data;
    private List<Edge> edges; 

    public Node(String data) {
        this.data = data;
        this.edges = new ArrayList<>();
    }

    public void addEdge(Edge edge){
        edges.add(edge);
    }

    public void removeEdge(Edge edge){
        edges.remove(edge);
    }

    public void removeEdge(Node node){
        Edge edgeToRemove = new Edge(node, 0);

        for(Edge edge : edges){
            if(edge.getDestination() == node){
                edgeToRemove = edge;
            }
        }

        removeEdge(edgeToRemove);
    }

    public String getData(){
        return data;
    }

    public List<Edge> getEdges(){
        return edges;
    }

    public void printEdges(){
        for(Edge edge : edges){
            System.out.println(edge.getDestination().data + " " + edge.getWeight());
        }
    }
}
